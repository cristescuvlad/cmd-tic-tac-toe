#include <iostream>

using namespace std;

void showGameBoard(char GameBoard[3][3])
{

for (int i = 0; i < 3; i++)
	{
	    cout<<"\n";
	    cout<<"|";
		for (int j = 0; j < 3; j++)
		{
			cout<<" "<<GameBoard[i][j]<<" | ";
		}
		cout<<"\n";
		cout<<" ______________ \n";
	}
}

void placeX(char GameBoard[3][3])
{
    int x,y;
	std::cout << "Introduceti coordonatele unde doriti sa plasati X-ul: \n";
    std::cin >> x >> y;
    std::cout << "\n";
    if (GameBoard[x][y] == ' '&&GameBoard[x][y] != '0'&&x<3&&y<3&&x>=0&&y>=0)
    {
        GameBoard[x][y] = 'X';
    }
    else
    {
        cout<<"Nu ati introdus corect, mai incercati!\n";
        placeX(GameBoard);
    }
}

void place0(char GameBoard[3][3])
{
    int x,y;
    std::cout << "Introduceti coordonatele unde doriti sa plasati 0-ul: \n";
    std::cin >> x >> y;
    std::cout << "\n";
    if (GameBoard[x][y] == ' '&&GameBoard[x][y] != 'X'&&x<3&&y<3&&x>=0&&y>=0)
    {
        GameBoard[x][y] = '0';
    }
    else
    {
        cout<<"Nu ati introdus corect, mai incercati!\n";
        place0(GameBoard);
    }
}


bool Win(char GameBoard[3][3])
{
    if((GameBoard[0][0]=='0'&&GameBoard[0][1]=='0'&&GameBoard[0][2]=='0')||
    (GameBoard[1][0]=='0'&&GameBoard[1][1]=='0'&&GameBoard[1][2]=='0')||
    (GameBoard[2][0]=='0'&&GameBoard[2][1]=='0'&&GameBoard[2][2]=='0')||
    (GameBoard[0][0]=='X'&&GameBoard[0][1]=='X'&&GameBoard[0][2]=='X')||
    (GameBoard[1][0]=='X'&&GameBoard[1][1]=='X'&&GameBoard[1][2]=='X')||
    (GameBoard[2][0]=='X'&&GameBoard[2][1]=='X'&&GameBoard[2][2]=='X')||
    // pe linii
    (GameBoard[0][0]=='0'&&GameBoard[1][0]=='0'&&GameBoard[2][0]=='0')||
    (GameBoard[0][1]=='0'&&GameBoard[1][1]=='0'&&GameBoard[2][1]=='0')||
    (GameBoard[0][2]=='0'&&GameBoard[1][2]=='0'&&GameBoard[2][2]=='0')||
    (GameBoard[0][0]=='X'&&GameBoard[1][0]=='X'&&GameBoard[2][0]=='X')||
    (GameBoard[0][1]=='X'&&GameBoard[1][1]=='X'&&GameBoard[2][1]=='X')||
    (GameBoard[0][2]=='X'&&GameBoard[1][2]=='X'&&GameBoard[2][2]=='X')||
    // pe coloane
    (GameBoard[0][0]=='0'&&GameBoard[1][1]=='0'&&GameBoard[2][2]=='0')||
    (GameBoard[0][2]=='0'&&GameBoard[1][1]=='0'&&GameBoard[2][0]=='0')||
    (GameBoard[0][0]=='X'&&GameBoard[1][1]=='X'&&GameBoard[2][2]=='X')||
    (GameBoard[0][2]=='X'&&GameBoard[1][1]=='X'&&GameBoard[2][0]=='X')
    /* pe diagonale*/)
    return true;
    else return false;
}

int main()
{
    char GameBoard[3][3];
    int turn;
    bool playing;

    for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			GameBoard[i][j]= ' ';
		}
	}

	turn=1;
	showGameBoard(GameBoard);
	while(turn<=9)
    {

        if(turn%2==1)
          {
              placeX(GameBoard);
              showGameBoard(GameBoard);
          }
        else
        {
          place0(GameBoard);
          showGameBoard(GameBoard);
        }
        turn++;
        if(Win(GameBoard)==true)
        {
            cout<<"\n Felicitari! ";
            if(turn%2==1)
            {
                cout<<"Jucatorul 2 a castigat!\n";
            }
            else
            {
                cout<<"Jucatorul 1 a castigat!\n";
            }
            return 0;
        }
    }
    cout<<"Remiza!";
    return 0;

}
